# La proposition

[Çi-git](https://gitlab.com/guido-o/paperjam-2021/), ici-même, du 25 au 30 mai, un jam collectif pour s'emparer de la [ZAP Rimbaud](https://antilivre.gitlab.io/rimbaud.zap/), sur [son propre terrain](https://gitlab.com/antilivre/rimbaud.zap) [git] pour mieux saisir ce qui s'y est joué. Nous partirons de [ce ·](https://smonjour.gitpages.huma-num.fr/litterature-git/) avec l'ambition de nous approprier ce que la *[gittérature](https://www.cyberpoetique.org/gitterature/)* nous dit -- nous inspire surtout, des *écritures dispositives* . Parce que cet [appel à curiosités](https://aberrationsnumeriques.pubpub.org/) fait déjà écho à [nos hypothèses](https://these.nicolassauret.net), à [nos manifestes](http://www.sens-public.org/articles/1522/), à [nos actions](http://notes.ecrituresnumeriques.ca/2uMYqwveTg-Uy90ELVarfg.html) ou encore à [nos pratiques](http://ateliers.sens-public.org/) vers des fabriques alternatives des savoirs.

- Notre point de départ : [gitterature.md](gitterature.md)
- Nos règles du jeu : [protocole.md](protocole.md)

*Note des auteur.e.s*

En 2019, dans un café montréalais, Guido est né de notre lassitude à l'égard des injonctions auxquelles les chercheur.e.s sont soumis : publier, communiquer, signer, s'autoriser, se faire un nom, imposer sa "marque". Guido, héritier de Pédauque, mais aussi cousin de Wu-Ming ou du Général Instin, a l'ambition de devenir une signature collective de manière à lutter contre la singularisation des "auteurs" dans le milieu de la recherche. Parce qu'on ne pense jamais seul.e.s, mais que l'on signe trop souvent d'un seul nom (et que ce sont d'ailleurs souvent les mêmes qui finissent par imposer leur nom).

![](https://media.giphy.com/media/11W37uI72pjDkk/source.gif)

---

## Howto

- Produire le html: `pandoc --standalone  -f markdown -t html --citeproc --bibliography=gitterature.bib gitterature.md gitterature.yaml -o gitterature.html
`
